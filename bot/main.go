package main

import (
	"encoding/json"
	"fmt"
	"github.com/matrix-org/gomatrix"
	/* "github.com/russross/blackfriday" */
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

type configuration struct {
	Homeserver string `json:"homeserver"`
	Username   string `json:"username"`
	Password   string `json:"password"`
}

var config configuration

/*
TODO: set up fork of gomatrix that supports formatting

func md2html(bytes []byte) []byte {
	return blackfriday.MarkdownBasic(bytes)
}
*/

func pullCoordFile() ([]byte, error) {
	response, err := http.Get("https://gitlab.com/nocaps/brown-bricks/-/raw/master/coords")
	if err != nil {
		return []byte{}, err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return []byte{}, err
	}

	return body, nil
}

func init() {
	fileReader, err := os.Open("./config.json")

	defer fileReader.Close()

	fileBytes, err := ioutil.ReadAll(fileReader)
	if err != nil {
		log.Fatal(err)
	} else {
		json.Unmarshal(fileBytes, &config)
	}
}

func main() {
	client, err := gomatrix.NewClient(config.Homeserver, "", "")
	if err != nil {
		log.Fatal(err)
	}
	login, err := client.Login(&gomatrix.ReqLogin{
		Type:     "m.login.password",
		User:     config.Username,
		Password: config.Password,
	})
	if err != nil {
		log.Fatal(err)
	}

	client.SetCredentials(login.UserID, login.AccessToken)
	client.SetDisplayName("Inspector Gadget")

	syncer := client.Syncer.(*gomatrix.DefaultSyncer)
	syncer.OnEventType("m.room.message",

		func(ev *gomatrix.Event) {
			msg, ok := ev.Body()
			if ok {
				if strings.TrimSpace(msg) == "!coords" {
					coordinates, err := pullCoordFile()
					if err != nil {
						client.SendText(ev.RoomID, "Error: "+err.Error())
					} else {
						client.SendText(ev.RoomID, string(coordinates))
					}
				}
			}

		})

	if err := client.Sync(); err != nil {
		fmt.Println("Sync() returned ", err)
	}
}
